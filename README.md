test-work
=========
    устанавливаем компосер
    php -r "readfile('https://getcomposer.org/installer');" | php
    
    обновляем симфони 
    php composer.phar install
    
    sudo chown www-data:www-data /путь к папке
    
    sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs  \n <br>
    
    sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs  \n <br>
    
    sudo php app/console doctrine:database:create
    sudo php app/console doctrine:schema:update --force 
    sudo php app/console assets:install --symlink 