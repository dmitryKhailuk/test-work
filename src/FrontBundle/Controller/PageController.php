<?php

namespace FrontBundle\Controller;

use FrontBundle\Entity\File;
use FrontBundle\Entity\Message;
use FrontBundle\Service\Tool;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FrontBundle\Form\MessageFormType;
use ZipArchive;

class PageController extends Controller
{
    /**
     * @Route("/", name="page_index")
     */
    public function indexAction()
    {

        return $this->redirect($this->generateUrl('page_contact_us'));
    }

    /**
     * @Route("/contact-us", name="page_contact_us")
     * @Template()
     */
    public function contactUsAction()
    {
        $Message = new Message();
        $form = $this->createForm(MessageFormType::class, $Message);
        return array(
            'form_contact' => $form->createView()
        );
    }

    /**
     * @Route("/all-request", name="page_all_request")
     * @Template()
     */
    public function allRequestAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $all_requests = $em->getRepository('FrontBundle:Message')->findAll();
        return array(
            'all_requests' => $all_requests,
            'error'        => $request->get('error')
        );
    }

    /**
     * @Route("/upload-data", name="page_upload_data")
     */
    public function uploadDataAction(Request $request)
    {
        $Message = new Message();
        $form = $this->createForm(MessageFormType::class, $Message);
        $form->handleRequest($request);
        $message_array = $request->get('message_form');
        if ( (isset($message_array['name']) && !empty($message_array['name']) )
                && (isset($message_array['email']) && !empty($message_array['email']) )
                && (isset($message_array['message']) && !empty($message_array['message']) ) ) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Message);
            foreach ($request->files as $uploadFile) {
                $check_file_type = Tool::CheckFileType($uploadFile->getMimeType());
                if ($check_file_type) {
                    $File = new File();
                    $File->setFile($uploadFile);
                    $File->setMessage($Message);
                    $File->setName($uploadFile->getClientOriginalName());
                    $File->setPath(md5(rand(10000, 99999)).$uploadFile->getClientOriginalName());
                    $File->upload();
                    $em->persist($File);
                } else {
                    return new Response(json_encode(array('mess' => 'bad_upload_file')));
                }
            }
            $em->flush();
            return new Response(json_encode(array('mess' => 'ok')));
        } else {
            return new Response(json_encode(array('mess' => 'error')));
        }

        return new Response('error');
    }

    /**
     * @Route("/download-file", name="page_download_file")
     * @Method("GET")
     */
    public function downloadFileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $file = $em->getRepository('FrontBundle:File')->findOneById($id);
        if ($file) {
            Tool::getFile($file->getAbsolutePath(), $file->getName());
        }
        return $this->redirect($this->generateUrl('page_all_request', array('error' => 'download') ));
    }

    /**
     * @Route("/download-allfile", name="page_download_all_file")
     * @Method("GET")
     */
    public function downloadAllFileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $file = $em->getRepository('FrontBundle:File')->allFileByRequest($id);
        $filepath = $request->server->get('DOCUMENT_ROOT');
        if (count($file) > 0 ) {
            $filename = $filepath.'/'.time().'allfile.zip';
            $zip = new \ZipArchive();
            if ($zip->open($filename, ZipArchive::CREATE) !== TRUE ) {
                return $this->redirect($this->generateUrl('page_all_request', array('error' => 'download') ));
            }
            foreach ($file as $value) {
                if ( file_exists($value->getAbsolutePath()) ) {
                    $zip->addFile($value->getAbsolutePath(), $value->getName());
                }
            }
            $zip->close();
            Tool::getFile($filename, basename($filename));
        }
        return $this->redirect($this->generateUrl('page_all_request', array('error' => 'download') ));

    }


    /**
     * @Route("/{url}", name="wrong_route", requirements={"url" = ".+"})
     *
     */
    public function wrongRouteAction($url){
        throw new NotFoundHttpException();
    }
}
