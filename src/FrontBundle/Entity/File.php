<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FrontBundle\Model\UploadModel;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Table(name="files")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\FileRepository")
 */
class File extends UploadModel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    protected $path;

    /**
     * @var string
     * @Assert\File( maxSize = "5M", mimeTypes= {"text/plain", "text/csv", "application/csv", "text/excel", "application/excel", "image/png", "image/jpg"})
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var UploadedFile
     * @Assert\File( maxSize = "5M", mimeTypes= {"text/plain", "text/csv", "application/csv", "text/excel", "application/excel", "image/png", "image/jpg"})
     */
    protected $file;

    /**
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="files")
     * @ORM\JoinColumn(name="message", referencedColumnName="id")
     **/
    protected $message;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set message
     *
     * @param \FrontBundle\Entity\Message $message
     * @return File
     */
    public function setMessage(\FrontBundle\Entity\Message $message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \FrontBundle\Entity\Message 
     */
    public function getMessage()
    {
        return $this->message;
    }
}
