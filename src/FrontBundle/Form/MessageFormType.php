<?php


namespace FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;


class MessageFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('name', null, array('attr' => array('placeholder'=>'')))
            ->add('email', 'email', array('attr' => array('placeholder'=>'' )))
            ->add('message', 'textarea', array('attr' => array('placeholder'=>'')));
    }

}
