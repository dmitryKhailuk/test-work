function ActiveMenu() {
    $child = $('.menubase').children();
    $child.each(function(){
        $(this).removeClass('active');
    });
}

function validateEmail(email) {
    var reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return reg.test(email);
}

$(document).ready(function() {
    $('.alert').on('click', '.close', function(e) {
        var $alert = $(e.delegateTarget);
        if ($alert.is(':hidden') ) {
            $alert.slideDown();
        } else {
            $alert.slideUp();
        }
    });
});
