<?php

namespace FrontBundle\Service;


class Tool {

    public static function getFile($path, $filename) {
        if (file_exists($path)) {
            if (ob_get_level()) {
                ob_end_clean();
            }
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            readfile($path);
            exit;
        }
        return false;
    }
    public static function CheckFileType($type) {
        if (in_array($type, array('text/plain','text/csv', 'application/csv', 'application/excel', 'image/png', 'image/jpg', 'image/jpeg', 'text/excel', 'application/pdf', 'application/x-pdf' ))) {
            
            return true;
        } else {
            return false;
        }
    }

}
