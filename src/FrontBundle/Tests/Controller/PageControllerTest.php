<?php

namespace FrontBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PageControllerTest extends WebTestCase
{

    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        return array(
            array('/en'),
            array('/en/contact-us'),
            array('/en/all-request'),
            array('/ru'),
            array('/ru/contact-us'),
            array('/ru/all-request'),
        );
    }

    public function testContactUs()
    {
        $client = self::createClient();
        $url = $client->getContainer()->get('router')->generate('page_download_file');
        $client->request('GET', $url);
    }
}
